import React from 'react';
import '../App.css';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Box from '@material-ui/core/Box';

class MotorRelay extends React.Component {

    constructor(props) {
        super(props);
        this.props = {
            // Raw relay output
            right_relay_1: false,
            right_relay_2: false,
            left_relay_1: false,
            left_relay_2: false,

        };
    }

    // Sends the clicked event from the input and output
    checked(event) {
    //        this.sendMessage("[{ \"and\": { \""  + event.target.name + "\": " + event.target.checked + " } }]");
    }

    render() { return (
        <Box minWidth={180} maxWidth={180} p={2} m={1}  border={2} borderRadius={5} boxShadow={3} bgcolor="azure" >
            <FormGroup >
                <FormGroup row >
                    <FormLabel >Drive Motor Relays</FormLabel>
                </FormGroup>
                <FormControlLabel
                    control={<Switch name="right_relay_1" style={{ animation: 'none', transition: 'none' }} checked={this.props.right_relay_1} onChange={ (event) => { this.checked(event); }}/>}
                    label="Right Relay 1"
                />
                <FormControlLabel
                    control={<Switch name="right_relay_2" style={{ animation: 'none', transition: 'none' }} disableRipple={true} checked={this.props.right_relay_2} onChange={ (event) => { this.checked(event); }}/>}
                    label="Right Relay 2"
                />
                <FormControlLabel
                    control={<Switch name="left_relay_1"  style={{ animation: 'none', transition: 'none' }} disableRipple={true} checked={this.props.left_relay_1} onChange={ (event) => { this.checked(event); }}/>}
                    label="Left Relay 1"
                />
                <FormControlLabel
                    control={<Switch name="left_relay_2"  style={{ animation: 'none', transition: 'none' }} disableRipple={true} checked={this.props.left_relay_2} onChange={ (event) => { this.checked(event); }}/>}
                    label="Left Relay 2"
                />
            </FormGroup>
        </Box>)
    }
}

export default MotorRelay;
