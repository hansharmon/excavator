import React from 'react';
import './App.css';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import ArrowBackIcon     from '@material-ui/icons/ArrowBack';
import ArrowUpwardIcon   from '@material-ui/icons/ArrowUpward';
import ArrowForwardIcon  from '@material-ui/icons/ArrowForward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import StopRoundedIcon   from '@material-ui/icons/StopRounded';
import Websocket from 'react-websocket';
import Box from '@material-ui/core/Box';
import MotorRelay from './components/MotorRelay';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.onMessage = this.onMessage.bind(this)
        this.state = {
            right_relay_1: false,
            right_relay_2: false,
            left_relay_1: false,
            left_relay_2: false,
//Color of the arrows indicating wheel motion
            up:      "disabled",
            down:    "disabled",
            left:    "disabled",
            right:   "disabled",
            stopped: "secondary"

        };
    }


    onMessage (json) {
        /// Ignore this echos of the set values.
        if (json.startsWith("{ \"root\": { \"values\":")) return;
        console.log(json);
        var object = JSON.parse(json);

        for ( var nanite in object.root ) {
            if ( nanite === "gpio") {
              for ( var node in object.root[nanite] ) {
                  var kv = "{ \"" + node + "\": " + object.root[nanite][node] + " }";
//                  console.log(kv);
                  var kvo = JSON.parse(kv);
                  this.setState (kvo);
              }
            }
        }
    }

    sendMessage(msg) {
        console.log("Sending " + msg);
        this.refWebSocket.sendMessage(msg);
    }

    // Keep the socket open by sending a message.
    onOpen() {
        setInterval(() => {
            this.sendMessage('[]');
        }, 10000 );
    }

    render() {
        // Need this to be the equivalent of "ws://" + self.location.host + "/control"
//    let my_url = "ws://" + window.location.host + "/control";
//        let my_url = "ws://" + window.location.hostname  + ":8125/control";
        let my_url   = "ws://192.168.0.18:8125/controls";     // Switch to the correct above in the future

        return (
            <div>
                <Websocket url={my_url}
                           onMessage={this.onMessage}
                           onOpen={() => { this.onOpen() }}
                           ref={ Websocket => { this.refWebSocket = Websocket; }}
                />
                <Box m={1}>
                <FormGroup row>
                    <FormGroup >
                        <Box minWidth={130} maxWidth={130} border={2} borderRadius={5} p={2} m={1} boxShadow={3} bgcolor="azure">
                            <FormGroup >
                                <FormGroup row >
                                    <FormLabel >Current Direction</FormLabel>
                                </FormGroup>
                                <FormGroup row >
                                    <StopRoundedIcon color="disabled" style={{ fontSize: 40 }} />
                                    <ArrowUpwardIcon name="up_arrow" color={this.state.up} style={{ fontSize: 40 }} />
                                    <StopRoundedIcon color="disabled" style={{ fontSize: 40 }} />
                                </FormGroup >
                                <FormGroup row >
                                    <ArrowBackIcon    name="left_arrow"  color={this.state.left} style={{ fontSize: 40 }} />
                                    <StopRoundedIcon  color={this.state.stopped} style={{ fontSize: 40 }} />
                                    <ArrowForwardIcon name="right_arrow" color={this.state.right} style={{ fontSize: 40 }} />
                                </FormGroup >
                                <FormGroup row >
                                    <StopRoundedIcon   color="disabled" style={{ fontSize: 40 }} />
                                    <ArrowDownwardIcon name="down_arrow" color={this.state.down} style={{ fontSize: 40 }} />
                                    <StopRoundedIcon   color="disabled" style={{ fontSize: 40 }} />
                                </FormGroup >
                            </FormGroup >
                        </Box>
                        <MotorRelay
                            right_relay_1={this.state.right_relay_1}
                            right_relay_2={this.state.right_relay_2}
                            left_relay_1={this.state.left_relay_1}
                            left_relay_2={this.state.left_relay_2}
                        />
                    </FormGroup>

                    <Box minWidth={330} maxWidth={330} maxHeight={250}  p={2} m={1}  border={2} borderRadius={5} boxShadow={3} bgcolor="azure" >
                        <iframe src="http://192.168.0.18:8125/" allowfullscreen="true" height="500" width="650" style={{ "transform": "scale(0.5)", "transformOrigin" : "0 0" }} />
                    </Box>
                </FormGroup>
                </Box>
            </div>
        );
    }
}
export default App;
