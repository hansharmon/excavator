# Excavator

![Excavator Current UX](https://bitbucket.org/hansharmon/excavator/raw/180dfabd255771093048df52935462805db25d8b/gui.png)

This is the excavator project based on Nanite and was an example.  The project is being broken out to demonstrate a full application of nanite and how to use. 
Excavator is to run on a pi and control a power wheels excavator with ReactJS front end for viewing controlling the excavator.
The excavator can also be used manually.  All data is stored in and InFluxDB and can be 

## Nanite Project:
https://bitbucket.org/hansharmon/nanite/
Install nanite on the pi and use the 'nanited' command to add the excavator as a daemon on startup



## Video:
https://www.youtube.com/watch?v=2aWamjqahPc


## Startup as excavator:
To have the excavator start on startup of the PI, nanite provides a nanited script for turning a .json script into a init.d script for the PI.

Example:  sudo nanited excavator /home/pi/excavator excavator.json


